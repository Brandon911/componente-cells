class todasTareas extends HTMLElement{
    constructor(){
        super();
        this.tarea;
    }

    static get observedAttributes(){
        return ['tarea'];
    }

    attributeChangedCallback(tareaAttr, oldValue, newValue){
        switch(tareaAttr){
            case "tarea":
                this.tarea = newValue;
            break;
        }
    }

    connectedCallback(){
        this.innerHTML = `<div>
            <h2> ${this.tarea}</h2>
        </div>`;
        this.style.color = "black";
        this.style.fontFamily = "arial";
    }
}
window.customElements.define("tareas-lista", todasTareas);
let parameters = []
function removeElement(event, position) {
    event.target.parentElement.remove()
    delete parameters[position]
}
const addJsonElement = json => {
    parameters.push(json)
    return parameters.length - 1
}
(function load(){
    const $form = document.getElementById("frmUsers")
    const $divElements = document.getElementById("divElements")
    const $btnSave = document.getElementById("btnSave")
    const $btnAdd = document.getElementById("btnAdd")
    const templateElement = (data, position) => {
        return (`
            <button class="delete" onclick="removeElement(event, ${position})"></button>
            <strong>Tarea - </strong> ${data}`)
    }
    $btnAdd.addEventListener("click", (event) => {
        if($form.name.value != ""){
            let index = addJsonElement({
                name: $form.name.value,
            })
            const $div = document.createElement("div")
            $div.classList.add("notification", "is-link", "is-light", "py-2", "my-1")
            $div.innerHTML = templateElement(`${$form.name.value} `, index)
            $divElements.insertBefore($div, $divElements.firstChild)
            $form.reset()
        }else{
            alert("Introduzca alguna tarea")
        }
    })
})()